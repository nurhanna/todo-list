import { combineReducers } from 'redux';

import appReducer from '@containers/App/reducer';
import languageReducer, { storedKey as storedLangState } from '@containers/Language/reducer';

import { mapWithPersistor } from './persistence';
import todoReducer, { storedKey as storedTodoState } from '@containers/Todo/reducer';

const storedReducers = {
  language: { reducer: languageReducer, whitelist: storedLangState },
  appTodo: { reducer: todoReducer, whitelist: storedTodoState },
};

const temporaryReducers = {
  app: appReducer,
};

const createReducer = () => {
  const coreReducer = combineReducers({
    ...mapWithPersistor(storedReducers),
    ...temporaryReducers,
  });
  const rootReducer = (state, action) => coreReducer(state, action);
  return rootReducer;
};

export default createReducer;
