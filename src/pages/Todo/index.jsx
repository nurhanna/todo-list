import classes from './style.module.scss';
import { useState, useMemo } from 'react';

// mui
import DarkModeIcon from '@mui/icons-material/DarkMode';
import LightModeIcon from '@mui/icons-material/LightMode';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import ClearIcon from '@mui/icons-material/Clear';
import EditIcon from '@mui/icons-material/Edit';
import Button from '@mui/material/Button';

// for redux
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { useDispatch } from 'react-redux';

// selector
import { selectTodo } from '@containers/Todo/selectors';

// action
import { setTodo } from '@containers/Todo/actions';
import { updateTodo } from '@containers/Todo/actions';
import { deleteTodo } from '@containers/Todo/actions';
import { clearCompleted } from '@containers/Todo/actions';
import { updateTodoTask } from '@containers/Todo/actions';
import { v4 as uuid } from 'uuid';

// dnd
import { moveTodo } from '@containers/Todo/actions';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

// theme
import { selectTheme } from '@containers/Todo/selectors';
import { setMode } from '@containers/Todo/actions';

const Todo = ({ todo, mode }) => {
  const dispatch = useDispatch();

  //handle input new task
  const handleKeyDown = (event) => {
    if (event.key === 'Enter') {
      const newTodo = {
        id: uuid(),
        title: event.target.value,
        completed: false,
      };
      console.log('tambah todo baru');
      dispatch(setTodo(newTodo)) ? (event.target.value = '') : (event.target.value = event.target.value);
    }
  };

  // handle update task completed or uncompleted
  const handleCheck = (id, completed) => {
    dispatch(updateTodo(id, completed));
  };

  //handle delete task
  const handleDelete = (idx) => {
    dispatch(deleteTodo(idx));
  };

  // handle clear completed task
  const handleClearCompleted = () => {
    dispatch(clearCompleted());
  };

  // handle theme
  const handleTheme = () => {
    dispatch(setMode(mode === 'light' ? 'dark' : 'light'));
  };
  const [itemLeft, setItemLeft] = useState(todo.length);
  const [filter, setFilter] = useState('all');
  const filteredData = useMemo(() => {
    if (filter === 'active') {
      return todo.filter((item) => {
        return item.completed === false;
      });
    }
    if (filter === 'complete') {
      return todo.filter((item) => {
        return item.completed === true;
      });
    }
    return todo;
  }, [filter, todo]);

  // handle select menu/filter
  useMemo(() => {
    setItemLeft(filteredData.length);
  }, [filteredData]);

  // handle when click edit
  const [idUpdate, setIdUpdate] = useState(null);
  const [taskUpdate, setTaskUpdate] = useState('');
  const handleClickEdit = (id, title) => {
    setIdUpdate(id);
    setTaskUpdate(title);
  };

  // handle edit task
  const handleEdit = () => {
    dispatch(updateTodoTask(idUpdate, taskUpdate));
    setIdUpdate(null);
    setTaskUpdate('');
  };

  // function to reorder the tasks after drag and drop
  const reorderTasks = (sourceId, destinationId, array) => {
    const sourceIndex = array.findIndex((item) => item.id === sourceId);
    const destinationIndex = array.findIndex((item) => item.id === destinationId);

    if (sourceIndex === -1 || destinationIndex === -1) {
      return array;
    }

    const newArray = [...array];
    const [removedItem] = newArray.splice(sourceIndex, 1);
    newArray.splice(destinationIndex, 0, removedItem);
    return newArray;
  };

  // if list drag and drop
  const handleDragEnd = (result) => {
    if (!result.destination) {
      return;
    }
    const sourceId = filteredData[result.source.index].id;
    const destinationId = filteredData[result.destination.index].id;
    const newTodos = reorderTasks(sourceId, destinationId, todo);

    dispatch(moveTodo(newTodos));
  };

  // get data all or after filter based on filter state
  const handleData = () => {
    return (
      <div>
        <DragDropContext onDragEnd={handleDragEnd}>
          <Droppable droppableId="droppable">
            {(provided) => (
              <div ref={provided.innerRef} {...provided.droppableProps}>
                {filteredData.map((item, index) =>
                  idUpdate && item.id === idUpdate ? (
                    <ListItem disablePadding key={index} className={classes.listData}>
                      <ListItemButton>
                        <ListItemIcon></ListItemIcon>
                        <div className={classes.editBox}>
                          <input
                            className={classes.inputEdit}
                            type="text"
                            value={taskUpdate}
                            onChange={(e) => setTaskUpdate(e.target.value)}
                          />
                          <Button onClick={() => setIdUpdate(null)}>Cancel</Button>
                          <Button onClick={handleEdit}>Save</Button>
                        </div>
                      </ListItemButton>
                    </ListItem>
                  ) : (
                    <Draggable key={item.id} draggableId={item.id} index={index} isDragDisabled={idUpdate !== null}>
                      {(provided) => (
                        <div ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                          <ListItem disablePadding key={index} className={classes.listData}>
                            <ListItemButton>
                              <ListItemIcon>
                                <input
                                  type="checkbox"
                                  className={classes.checkbox}
                                  checked={item.completed === true ? true : false}
                                  onChange={() => handleCheck(item.id, item.completed === true ? false : true)}
                                ></input>
                              </ListItemIcon>
                              <ListItemText primary={item.title} className={item.completed ? classes.coret : null} />
                              <Button onClick={() => handleClickEdit(item.id, item.title)}>{<EditIcon />}</Button>
                              <Button onClick={() => handleDelete(item.id)}>{<ClearIcon />}</Button>
                            </ListItemButton>
                          </ListItem>
                        </div>
                      )}
                    </Draggable>
                  )
                )}
                {provided.placeholder}
              </div>
            )}
          </Droppable>
        </DragDropContext>
      </div>
    );
  };

  return (
    <div className={mode === 'light' ? classes.light : classes.dark}>
      <div className={classes.containerCenter}>
        <div className={classes.containerheader}>
          <div className={classes.logo}>TODO</div>
          <div className={classes.icon}>
            <Button variant="text" onClick={() => handleTheme()} className={classes.modeButton}>
              {mode === 'light' ? <DarkModeIcon /> : <LightModeIcon />}
            </Button>
          </div>
        </div>
        <div className={classes.containerinput}>
          <input
            type="text"
            className={classes.input}
            placeholder=" Create a new todo"
            onKeyDown={handleKeyDown}
          ></input>
        </div>
        <div className={classes.containerList}>
          {filteredData.length > 0 ? (
            <List sx={{ width: '100%' }} aria-label="contacts" className={classes.listbox}>
              {handleData()}
            </List>
          ) : (
            <div className={classes.notask}>No Tasks Yet</div>
          )}
        </div>
        <div className={classes.footerList}>
          <div className={classes.menu}>{itemLeft} Item Left</div>
          <div
            className={classes.menu}
            onClick={() => setFilter('all')}
            style={filter === 'all' ? { color: 'blue' } : null}
          >
            All
          </div>
          <div
            className={classes.menu}
            onClick={() => setFilter('active')}
            style={filter === 'active' ? { color: 'blue' } : null}
          >
            Active
          </div>
          <div
            className={classes.menu}
            onClick={() => setFilter('complete')}
            style={filter === 'complete' ? { color: 'blue' } : null}
          >
            Completed
          </div>
          <div className={classes.menu} onClick={handleClearCompleted}>
            Clear Completed
          </div>
        </div>
        <div className={classes.footerListPhone}>
          <div className={classes.footerUp}>
            <div>{itemLeft} Item Left</div>
            <div onClick={handleClearCompleted}>Clear Completed</div>
          </div>
          <div className={classes.footerDown}>
            <div onClick={() => setFilter('all')} style={filter === 'all' ? { color: 'blue' } : null}>
              All
            </div>
            <div onClick={() => setFilter('active')} style={filter === 'active' ? { color: 'blue' } : null}>
              Active
            </div>
            <div onClick={() => setFilter('complete')} style={filter === 'complete' ? { color: 'blue' } : null}>
              Completed
            </div>
          </div>
        </div>
        <div className={classes.containerFooter}>Drag and drop to reorder list</div>
      </div>
    </div>
  );
};
Todo.propTypes = {
  todo: PropTypes.array,
  mode: PropTypes.string,
};

const mapStateToProps = createStructuredSelector({
  todo: selectTodo,
  mode: selectTheme,
});

const mapDispatchToProps = {
  setTodo,
  setMode,
  updateTodo,
  deleteTodo,
  clearCompleted,
  updateTodoTask,
};

export default connect(mapStateToProps, mapDispatchToProps)(Todo);
