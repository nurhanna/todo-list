import { SET_TODO } from '@containers/Todo/constants';
import { SET_MODE } from '@containers/Todo/constants';
import { UPDATE_TODO } from '@containers/Todo/constants';
import { DELETE_TODO } from '@containers/Todo/constants';
import { CLEAR_COMPLETED } from '@containers/Todo/constants';
import { UPDATE_TODO_TASK } from '@containers/Todo/constants';
import { MOVE_TODO } from '@containers/Todo/constants';

export function setTodo(todos) {
  console.log(todos);
  return {
    type: SET_TODO,
    todos,
  };
}

export function setMode(mode) {
  return {
    type: SET_MODE,
    mode,
  };
}

export function updateTodo(id, completed) {
  console.log('update pakai id : ', id);
  return {
    type: UPDATE_TODO,
    id,
    completed,
  };
}

export function deleteTodo(id) {
  console.log(id);
  return {
    type: DELETE_TODO,
    id,
  };
}

export function clearCompleted() {
  return {
    type: CLEAR_COMPLETED,
  };
}

export function updateTodoTask(id, task) {
  return {
    type: UPDATE_TODO_TASK,
    id,
    task,
  };
}

export const moveTodo = (todos) => {
  return {
    type: MOVE_TODO,
    todos,
  };
};
