import merge from 'lodash/merge';
import { produce } from 'immer';

import {
  SET_TODO,
  UPDATE_TODO,
  DELETE_TODO,
  CLEAR_COMPLETED,
  UPDATE_TODO_TASK,
  MOVE_TODO,
} from '@containers/Todo/constants';
import { SET_MODE } from '@containers/Todo/constants';

export const initialState = {
  todos: [],
  mode: 'light',
};

export const storedKey = ['todos'];

// eslint-disable-next-line default-param-last
const todoReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SET_TODO:
        draft.todos = [...draft.todos, action.todos];
        break;
      case SET_MODE:
        draft.mode = action.mode;
        break;
      case UPDATE_TODO:
        const index = draft.todos.findIndex((object) => {
          return object.id === action.id;
        });
        draft.todos[index].completed = action.completed;
        break;
      case DELETE_TODO:
        const indexOfObject = draft.todos.findIndex((object) => {
          return object.id === action.id;
        });
        draft.todos.splice(indexOfObject, 1);
        break;
      case CLEAR_COMPLETED:
        draft.todos = draft.todos.filter((item) => {
          return item.completed == false;
        });
        break;
      case UPDATE_TODO_TASK:
        const idxUpdate = draft.todos.findIndex((object) => {
          return object.id === action.id;
        });
        draft.todos[idxUpdate].title = action.task;
        break;
      case MOVE_TODO:
        draft.todos = action.todos;
        break;
      default:
        break;
    }
  });

export default todoReducer;
