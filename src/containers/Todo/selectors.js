import { createSelector } from 'reselect';

import { initialState } from '@containers/Todo/reducer';

const selectTodoState = (state) => state.appTodo || initialState;

export const selectTodo = createSelector(selectTodoState, (state) => state.todos);
export const selectTheme = createSelector(selectTodoState, (state) => state.mode);
