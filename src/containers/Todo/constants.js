export const SET_TODO = 'Todo/SET_TODO';
export const SET_MODE = 'Todo/SET_MODE';
export const UPDATE_TODO = 'Todo/UPDATE_TODO';
export const DELETE_TODO = 'Todo/DELETE_TODO';
export const CLEAR_COMPLETED = 'Todo/CLEAR_COMPLETED';
export const UPDATE_TODO_TASK = 'Todo/UPDATE_TODO_TASK';
export const MOVE_TODO = 'Todo/MOVE_TODO';
